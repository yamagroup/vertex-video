var mouseDown = false;
$("body").on('touchstart', function(event) {
  event.preventDefault();
  mouseDown = true;
});
$("body").on('touchmove', function(event) {
  event.preventDefault();
  if(mouseDown) {
    // Do something here.
  }
});
$(window.document).on('mouseup touchend', function(event) {
  // Capture this event anywhere in the document, since the mouse may leave our element while mouse is down and then the 'up' event will not fire within the element.
  mouseDown = false;
});

$(function() {
	FastClick.attach(document.body);
});

$(document).on("contextmenu",function(e){

    if( e.button == 2 ) {
        e.preventDefault();
    }
    return true;
});
