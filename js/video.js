document.getElementById('video').addEventListener("timeupdate", myfunc,false);

function myfunc(){
    if(this.currentTime > this.duration-5){
        //Less than 5 seconds to go. Do something here.

        //---- For Demo display purposes
        $(".coming-next").css("display","block");
        function redirect(){
            window.location.replace("videos.html");
        }
        setTimeout(redirect, 30000);
        //---------
    } //End Of If condition.
    //---- For Demo display purposes
    //---------
}

var supportsVideo = !!document.createElement('video').canPlayType;
if (supportsVideo) {
    var videoContainer = document.getElementById('videoContainer');
    var video = document.getElementById('video');
    var videoControls = document.getElementById('video-controls');

    // Hide the default controls
    video.controls = false;

    // Display the user defined video controls
    videoControls.style.display = 'block';

    var reload = document.getElementById('reload-video');
    var progress = document.getElementById('progress');
    var progressBar = document.getElementById('progress-bar');


    video.addEventListener('loadedmetadata', function() {
        progress.setAttribute('max', video.duration);
    });
    video.addEventListener('timeupdate', function() {
        progress.value = video.currentTime;
        progressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%';
    });
    video.addEventListener('timeupdate', function() {
        if (!progress.getAttribute('max')) progress.setAttribute('max', video.duration);
        progress.value = video.currentTime;
        progressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%';
    });
    progress.addEventListener('click', function(e) {
        var pos = (e.pageX  - this.offsetLeft) / this.offsetWidth;
        video.currentTime = pos * video.duration;
    });
    reload.addEventListener('click', function() {
        video.pause();
        video.currentTime = 0;
        progress.value = 0;
        video.play();
    });
}
